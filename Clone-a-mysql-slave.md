When you want to clone a MySQL serve to a new server for building a dev sever or set up relication or add a new node to cluster, you can use one of following methods:
- Use mysqldump if DB is small.
- Use innobackupdex or xtrabackup of Percona.
Details:
Task: Clone MySQL from server A to server B.
Steps: 
On server B
- Install tool PerconaXtrabackup, percona toolkit + pv(process view)+ pigz +nc
- Setup MySQL server as normal and stop service then
- rm -rf /var/lib/mysql/*
- nc -l -p 9999 |pv|unpigz -c | xbstream -xv -C /var/lib/mysql (use both innobackupdex and xtrabackup)
On server A:
- Install tools same above.
If use innobackupdex:
- innobackupex --defaults-file=/root/.my.cnf --no-timestamp --parallel=4 --close-files --stream=xbstream /data/tmp|pigz -c --fast |nc -w 2 10.3.49.13 9999
- IP is address of server that will be allowed to receive raw data. Port must match with one we confiugred on server A
If use xtrabackup:
- xtrabackup --backup --parallel=4 --stream=xbstream --target-dir=/data/tmp 2> backup.log |pv|pigz -c --fast| nc -w 2 10.3.49.13 9999
Wait for the progess is complete.
On server B:
- Innobackupdex: innobackupex --apply-log /var/lib/mysql
- Xtrabackup: xtrabackup --use-memory=1G --prepare --target-dir=/var/lib/mysql 2> prepare.log
- check file prepare(in case of xtrabaclup): completed OK! at lastline of log file.
- chown -R mysql. /var/lib/mysql and systemctl start mysql. Check that everything is ok.