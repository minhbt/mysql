#!/bin/bash
SOURCE_SERVER=$1
TARGET_SERVER=$2
# This is if you have /var/lib/mysql on the / (Root) drive.  You can change this to '/' to capture the size of the drive to get an idea of how long is left on your backup
MOUNT_CONTAINING_DATADIR='/var'
#Should match the number of CPU's on your server
BACKUP_THREADS=8
# Seconds to wait in the loop to check that the backup has completed
TIMER=5
# Amount of memory to use to apply the logs to the newly backed up server
MEMORY_GB_USED_APPLY_LOGS='10G'
# Change this to a 1 if you want to configure the target a slave of the source server
CONFIGURE_TARGET_AS_SLAVE_OF_SOURCE=1
GTID_STATUS=''
SSH_PORT="2395"
SSH="ssh -p $SSH_PORT -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
NETCAT_PORT=2112
REPL_USER='repl'
REPL_PASS='Adtech@2020'
MYSQL_DATADIR='/data/var/lib/mysql'
SOCKET='/data/var/lib/mysql/mysql.sock'
echo ""
echo "Starting MySQL Streaming Slave Rebuild Script"
echo ""
if [ ! $1 ];then
  echo "SOURCE Server not set.  Please run like :"
  echo ""
  echo "$0 <source_server> <target_server>"
  echo ""
  exit 1
fi
# VALIDATE EXECUTION
echo "Are you sure you wish to perform this rebuild.  THIS IS DESTRUCTIVE!!!"
echo "  SOURCE SERVER (Performing Backup) : $SOURCE_SERVER"
echo "  TARGET SERVER (Receiving Backup) : $TARGET_SERVER"
echo "    All files in $MYSQL_DATADIR on $TARGET_SERVER will be DELETED!!!"
echo -n "START NOW ? (y/n) : "
read CONFIRM
if [ $CONFIRM == "y" -o $CONFIRM == "Y" ]; then
  echo ""
  echo "STARTING REBUILD!!!"
else
  echo "Y or y was not chosen. Exiting."
  exit 1
fi
# PREPARE TARGET
echo "  Stopping MySQL"
$SSH $TARGET_SERVER "systemctl stop mysqld"
echo "  Clearing $MYSQL_DATADIR"
$SSH $TARGET_SERVER "rm -rf $MYSQL_DATADIR/*"
# PERFORM STREAMING BACKUP
echo "  Start Listener on Target server $TARGET_SERVER:$NETCAT_PORT to accept the backup and place it in $MYSQL_DATADIR"
$SSH $TARGET_SERVER "nc -l -p $NETCAT_PORT | unpigz -c | xbstream -x -C $MYSQL_DATADIR" >> /tmp/backup_log 2>&1
echo "  Starting backup on source server $SOURCE_SERVER:$NETCAT_PORT to stream backup"
$SSH $SOURCE_SERVER "innobackupex --stream=xbstream --parallel=$BACKUP_THREADS -S $SOCKET /data/tmp | pigz -c --fast | nc -w 2 $TARGET_SERVER $NETCAT_PORT" >> /tmp/backup_log 2>&1
sleep 4
echo "    Watching backup every $TIMER seconds to validate when the backup is complete"
LOOP=1
while [ 1 -eq $LOOP ];do
  BACKUP_PROCESSES=`$SSH $SOURCE_SERVER "ps aux | grep -v grep | grep -w innobackupex | wc -l"`
  if [ $BACKUP_PROCESSES -eq 0 ]; then
    echo "    Backup has COMPLETED!!"
    LOOP=2
  else
    echo "    Backup is Running!"
    sleep $TIMER
  fi
done
# PREPARE AND COMPLETE BACKUP ON TARGET
echo "  Applying logs to the Xtrabackup"
$SSH $TARGET_SERVER "innobackupex --use-memory=$MEMORY_GB_USED_APPLY_LOGS --apply-log $MYSQL_DATADIR" >> /tmp/apply_log 2>&1
sleep 3
LOOP=1
while [ 1 -eq $LOOP ];do
        APPLY_PROCESSES=`$SSH $TARGET_SERVER "ps aux | grep -v grep | grep innobackupex | wc -l"`
        if [ $APPLY_PROCESSES -eq 0 ]; then
                echo "    Apply logs has COMPLETED!!"
                LOOP=2
        else
                echo "    Apply Logs Running!"
                sleep $TIMER
        fi
done
sleep 1
echo "  Updating ownership on the backup files so that MySQL owns them"
$SSH $TARGET_SERVER "chown -R mysql:mysql $MYSQL_DATADIR"
echo "  Starting MySQL"
$SSH $TARGET_SERVER "systemctl start mysqld"
if [ $CONFIGURE_TARGET_AS_SLAVE_OF_SOURCE -eq 1 ]; then
  echo "  Configuring Replication"
  GTID_STATUS=`$SSH $SOURCE_SERVER "mysql -BN -e \"SHOW VARIABLES LIKE 'gtid_mode'\"" | grep -w ON | wc -l`
  if [ $GTID_STATUS -gt 0 ]; then
                echo "Found GTID ON.  Using Master Auto Position.  SLAVE STARTED"
                GTID_POS=`$SSH $TARGET_SERVER "cat $MYSQL_DATADIR/xtrabackup_binlog_info" | awk '{print $3}' | head -n 1 | sed 's/,//'`
                $SSH $TARGET_SERVER "mysql -e \"SET GLOBAL gtid_purged='$GTID_POS';\""
                $SSH $TARGET_SERVER "mysql -e \"CHANGE MASTER TO MASTER_HOST='$SOURCE_SERVER', MASTER_USER='$REPL_USER', MASTER_PASSWORD='$REPL_PASS', MASTER_AUTO_POSITION = 1; START SLAVE; \""
  else
    echo "Found GTID not ON.  Grabbing positioning from the backup file and using that to configure replication"
    MASTER_LOG=`$SSH $TARGET_SERVER "cat $MYSQL_DATADIR/xtrabackup_binlog_info" | awk '{print $1}'`
    MASTER_POS=`$SSH $TARGET_SERVER "cat $MYSQL_DATADIR/xtrabackup_binlog_info" | awk '{print $2}'`
    echo "Setting the slave to $MASTER_LOG and $MASTER_POS.  SLAVE STARTED"
    $SSH $TARGET_SERVER "mysql -e \"CHANGE MASTER TO MASTER_HOST='$SOURCE_SERVER', MASTER_USER='$REPL_USER', MASTER_PASSWORD='$REPL_PASS', MASTER_LOG_FILE='$MASTER_LOG', MASTER_LOG_POS=$MASTER_POS; START SLAVE;\""
  fi
fi